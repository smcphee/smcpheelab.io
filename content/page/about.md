---
title: About me
subtitle: why the fuck are you reading this anyway
comments: false
---

I'm a senior comp.sci for an airline. Technically my title is *Solutions Architect, Airline Operations* (but I still write code, because design is code is design). I specialise in airline operations, I love the deep arcana of *pushing metal* around the sky. Nowadays I'm writing mostly Javascript (React) and in the Akka framework for events / reactive programming in Java. We deploy our containers into AWS ECS (soon AKS).

I recently submitted my PhD dissertation (June 2019). My PhD is in the field of *Classics and Ancient History*. The title of my thesis is *Landscape and geographical space in T. Livius ab urbe condita*. Yeah there's Latin in it and I do my own translations, as a PhD student is expected. My favourite Latin poets are Ovid and Propertius.

I wrote my entire thesis in [pandoc/markdown](https://pandoc.org) and generated the PDF from that using LaTeχ, bibteχ, and xeteχ using `memoir`, using  gitlab.com ci-runners. I've got a complete `thesis-build` project which I will open source once I extract my actual thesis text from it.

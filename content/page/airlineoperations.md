## airline operations

>I've started to compile a range of interesting facts and opinions about [airline operations](.) pertinent to programming and systems design.

Airline operations? Yeah, airline operations. This page will eventually attempt to explain some of this stuff, especially as its relevant to systems design and application programming.

DISCLAIMER: **These are entirely my own observations and have nothing to do with my employer.**

## background

To get a good background, I would first recommend you watch [this excellent series of videos](https://www.youtube.com/playlist?list=PLqtidGFC6X0sv52gdw5i0TYHBShxHhldi) about transportation, which are from the [Wendover Productions](https://www.youtube.com/WendoverProductions) YouTube channel.

In particular:

* https://www.youtube.com/watch?v=6Oe8T3AvydU
* https://www.youtube.com/watch?v=-aQ2E0mlRQI
* https://www.youtube.com/watch?v=NlIdzF1_b5M
* https://www.youtube.com/watch?v=thqbjA2DC-E

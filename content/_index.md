## REMOVE THIS AND DIE

I'm a systems designer and programmer specialising in airline operations for a leading Australian airline. Also I've just completed a PhD @ The University of Queensland. See [page/about](./page/about/) for more information.

I live in Red Hill, QLD, AU.

I've started to compile a range of interesting facts and opinions about [airline operations](./page/airlineoperations/) pertinent to programming and systems design. These are entirely my own observations and have nothing to do with my employer.
